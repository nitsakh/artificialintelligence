package Solver;

import java.util.Scanner;

public class MainClass {
	public static void main(String args[]){
		
		/*Some sample puzzle states*/
		/*
		int IS1[][]={{2,1,3},{5,4,0},{6,7,8}};
		int IS[][]={{1,2,3},{4,5,6},{7,0,8}};
		int temp2[][]={{1,2,3},{4,5,6},{7,8,0}};
		int t[][]={{6,1,3},{0,7,4},{2,5,8}};
		int GS[][]={{1,2,3},{4,5,6},{7,8,0}};
		int nosol[][]={{1,2,3},{4,6,5},{7,8,0}};
		int t1[][]={{8,7,4},{6,3,2},{5,0,1}};
		int hard1[][]={{8,6,7},{2,5,4},{3,0,1}};
		int hard2[][]={{6,4,7},{8,5,0},{3,2,1}};
		int q1[][]={{2,8,3},{1,6,4},{7,0,5}};
		int q2[][]={{1,2,3},{8,0,4},{7,6,5}};
		*/
		int IS[][]=new int[3][3];
		int GS[][]=new int[3][3];
		Scanner s=new Scanner(System.in);
		
		System.out.println("Enter IS: ");
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				IS[i][j]=s.nextInt();
		
		System.out.println("Enter GS: ");
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				GS[i][j]=s.nextInt();
		
		
		
		Solve sol=new Solve(IS,GS);
						
		sol.solveItAstar();
		
	}
}
