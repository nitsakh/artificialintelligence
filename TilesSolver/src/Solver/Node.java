package Solver;

/*Node class
 * Class which implements one single state of the function.
 * It implements methods to find the manhattan distance.
 * It calculates value f=g+h
 * where f is the total cost to answer node
 * g is the cost up to current node, and
 * h is the manhattan distance
 */

public class Node {
	
	private int board[][]=new int[3][3];
	private int h=0;
	private int g=0;
	private int f=0;
	private Node parent=null;
	private int spacei=0,spacej=0;
	
	//Default Constructor
	public Node(){
		findSpace();
	}
	
	//Parameterized Constructor to copy arrays
	public Node(Node n){
		for (int i = 0; i < 3; i++) {
			System.arraycopy(n.getBoard()[i], 0, this.board[i], 0, 3);
		}
	}
	
	public Node(Node n,int b[][]){
		for (int i = 0; i < 3; i++) {
			System.arraycopy(b[i], 0, this.board[i], 0, 3);
		}
		this.parent=n;
		findSpace();
	}
	
	public void calcF(){
		f=g+h;
	}
	
	public void calcG(Node n){
		
		g=n.getG()+1;
	}
	
	
	//Implements Manhattan Distance Heuristic to calculate h value
	public void calcH(int gs[][]){
		
		int mdist=0;
		int temp,k,l=0;
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				temp=board[i][j];
				if(temp!=0){
				il:
				for(k=0;k<3;k++){
					for(l=0;l<3;l++){
						if(gs[k][l]==temp)
							break il;
					}
				}
					
					mdist+=Math.abs(k-i)+Math.abs(l-j);
				}
			}
		}
		h=mdist;
	}
	
	//Finds and stores the x,y location of the empty space on board
	public void findSpace(){
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				if(board[i][j]==0){
					spacei=i;
					spacej=j;
					break;
				}
			}
		}
	}
	
	//Exchanges tiles on moves
	public void swap(int i1,int j1,int i2,int j2){
		int temp;
		temp = board[i1][j1];
		board[i1][j1]=board[i2][j2];
		board[i2][j2]=temp;
	}
	
	//Determines if the goal has been reached
	public boolean isGoal(int gs[][]){
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				if(board[i][j]!=gs[i][j]){
					return false;
				}
			}
		}
				
		return true;
	}
	
	public void printState()
	{
		System.out.println(board[0][0] + " | " + board[0][1] + " | "
				+ board[0][2]);
		System.out.println("---------");
		System.out.println(board[1][0] + " | " + board[1][1] + " | "
				+ board[1][2]);
		System.out.println("---------");
		System.out.println(board[2][0] + " | " + board[2][1] + " | "
				+ board[2][2]);

	}
	
	
	public void setNode(Node temp){
	
		for (int i = 0; i < 3; i++) {
			for(int j=0;j<3;j++){
				this.board[i][j]=temp.getBoard()[i][j];
			}
		}
	}
	
	public Node getCur(){
		return this;
	}
	
	public int[][] getBoard(){
		return board;
	}
	public int getSpacei(){
		findSpace();
		return spacei;
	}
	
	public int getSpacej(){
		findSpace();
		return spacej;
	}
	
	public Node getParent(){
		return parent;
	}
	
	public int getG(){
		return g;
	}
	
	public int getF(){
		return f;
	}
	
	public int getH(){
		return h;
	}
}
