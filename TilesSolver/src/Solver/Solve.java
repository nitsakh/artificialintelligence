package Solver;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

/*Class Solve
 * Implements methods to implement A* Algorithm to solve the puzzle.
 * Generate moves generates moves and adds it to a list, which are 
 * later on added to a queue.
 */
public class Solve {
	
	private int IS[][]=new int[3][3];
	private int GS[][]=new int[3][3];
	
	//Default Constructor
	public Solve(){
		
	}
	
	//Parameterized constructor takes initial and final state of puzzle
	public Solve(int IS[][],int GS[][]){
		for (int i = 0; i < 3; i++) {
			for(int j=0;j<3;j++){
				this.IS[i][j]=IS[i][j];
				this.GS[i][j]=GS[i][j];
			}
		}

	}
	
	//Method to generate moves.
	//Returns array list of nodes of new moves
	private ArrayList<Node> genMoves(Node temp){
	
		ArrayList<Node> l=new ArrayList<Node>();
		l.clear();
		int spacei,spacej;
		spacei=temp.getSpacei();
		spacej=temp.getSpacej();
		//Move left
		if(spacej!=0){
			Node t=new Node(temp,temp.getBoard());
			t.swap(spacei, spacej, spacei, spacej-1);
			t.calcH(GS);
			t.calcG(temp);
			t.calcF();
			
			//t.printState();
			
			l.add(t);
		}
		//Move right
		if(spacej!=2){
			Node t=new Node(temp,temp.getBoard());
			t.swap(spacei, spacej, spacei, spacej+1);
			t.calcH(GS);
			t.calcG(temp);
			t.calcF();
			
			//t.printState();
			
			l.add(t);
		}
		//Move up
		if(spacei!=0){
			Node t=new Node(temp,temp.getBoard());
			t.swap(spacei, spacej, spacei-1, spacej);
			t.calcH(GS);
			t.calcG(temp);
			t.calcF();
			
			//t.printState();
			
			l.add(t);
		}
		//Move down
		if(spacei!=2){
			Node t=new Node(temp,temp.getBoard());
			t.swap(spacei, spacej, spacei+1, spacej);
			t.calcH(GS);
			t.calcG(temp);
			t.calcF();
			
			//t.printState();
			
			l.add(t);
		}
			
		return l;
	}
	
	//Optional BFS algorithm to solve puzzle
	public void solveItBFS(){
	
		Queue<Node> q=new LinkedList<Node>();
		Node root,temp,t;
		root=new Node(null,IS);
		q.add(root);
		while(!q.isEmpty()){
			temp=q.poll();
			if(!temp.isGoal(GS)){
				
				ArrayList<Node> succ=genMoves(temp);
				
				while(!succ.isEmpty()){
					t=succ.remove(0);
					if(!checkExists(t)){
						q.add(t);
						
					}
					
				}
				
			}
			else{
				Stack<Node> solution=new Stack<Node>();
				solution.push(temp);
				temp=temp.getParent();
				while(temp!=null){
					solution.push(temp);
					temp=temp.getParent();
				}
				
				int loopSize = solution.size();
				
				for (int i = 0; i < loopSize; i++)
				{
					temp=solution.pop();
					//temp.printState();
					//System.out.println();
					//System.out.println();
				}
				System.out.println("Number of moves required: "+(loopSize-1));
				break;
			}
			
		}
		
	}
	
	//Method to implement A* algorithm
	public void solveItAstar(){
		
		//Comparator for a priority queue to compare f values of states
		Comparator<Node> nodeComparator = new Comparator<Node>() {
	        @Override public int compare(Node n1, Node n2) {
	            return (n1.getF()>n2.getF()?1:(n1.getF()<n2.getF())?-1:0);
	        }           
	    };
		
	    //Priority Queue to obtain the node with least f cost
		PriorityQueue<Node> q=new PriorityQueue<>(2000, nodeComparator);
		int c=0;
		Node root,temp,t;
		root=new Node(null,IS);
		root.calcH(GS);
		root.calcF();
		q.add(root);
		while(!q.isEmpty()){
			temp=q.poll();
			c++;
			if(!temp.isGoal(GS)){
				
				ArrayList<Node> succ=genMoves(temp);
				int flag=0,mvcnt=succ.size();
				while(!succ.isEmpty()){
					t=succ.remove(0);
					if(!checkExists(t)){
						q.add(t);
						//t.printState();
						//System.out.println("\n");
						
					}
					else
						flag++;
					
				}
				if(flag==mvcnt){
					System.out.println("Nooooooooooo Solution !!");
					System.out.println("Number of states generated :: "+c);
					return;
				}
			}
			else{
				Stack<Node> solution=new Stack<Node>();
				solution.push(temp);
				temp=temp.getParent();
				while(temp!=null){
					solution.push(temp);
					temp=temp.getParent();
				}
				
				int loopSize = solution.size();
				if (loopSize > 0)
					System.out.println("\nMoves to Solution: ");
				for (int i = 0; i < loopSize; i++)
				{
					temp=solution.pop();
					temp.printState();
					System.out.println();
					System.out.println();
				}
				System.out.println("Number of moves required: "+(loopSize-1));
				break;
			}
			
		}
		
	}

	//Method to check if the current node has already been visited.
	public boolean checkExists(Node n){
		
		int tempb[][];
		int flag=0;
		Node temp=n;
		while((temp=temp.getParent())!=null){
			tempb=temp.getBoard();
			flag=0;
			for(int i=0;i<3;i++){
				for(int j=0;j<3;j++){
					if(tempb[i][j]!=n.getBoard()[i][j])
						flag=1;
				}
			}
			if(flag==0){
				return true;
			}
		}
		return false;
	}
	
}
