This Program uses the A* Algorithm to Implement the 8 tiles puzzle 
solver [Program also contains the BFS implementation, which may be
used if desired].
User has to enter the initial state of the puzzle as well as the 
final desired output state. After solving, the solution is provided
as a sequence of change in board positions, and the total number of
moves required.