
public class DepthFirstSearch {

	private int start;
	private Graph g;
	private int visited[]=new int[10];;
	
	public DepthFirstSearch(){
		
	}
	
	public DepthFirstSearch(Graph g,int start){
		this.start=start;
		this.g=g;
	}
	
	public void performDFS(){
		DFS(start);
	}
	
	public void DFS(int n){
		System.out.println("Node Visited: "+n);
		visited[n]=1;
		for(int i=0;i<g.n;i++){
			if(g.adjmat[i][n]==1 && visited[i]==0){
				DFS(i);
			}
		}
					
	}
}
