import java.util.*;

public class Graph {
	
	public int adjmat[][]=new int[10][10];
	public int n;
	Scanner s;
	
	public Graph(Scanner s){
		this.s=s;
		
	}
	
	public void acceptGraph(){
		
		System.out.println("Enter the number of vertices: ");
		n=s.nextInt();
		
		for(int i=0;i<n;i++){
			for(int j=0;j<i;j++){
				System.out.println("Does a node exist between "+ i + " and "+ j + "?(Y/N)");
				if(s.next().equalsIgnoreCase("Y")){
					addNode(i,j);
				}
			}
		}
		
	}
	
	public void addNode(int i, int j){
		adjmat[i][j]=1;
		adjmat[j][i]=1;
	}
	
	public void dispMat(){
		
		for(int i=0;i<n;i++){
			System.out.println("");
			for(int j=0;j<n;j++)
				System.out.print(adjmat[i][j]);
		}
	}
	
}
